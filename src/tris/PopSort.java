package tris;

import java.util.Comparator;

import jeux.CardLine;
import jeux.Carte;

public class PopSort implements Comparator<Carte>{

	@Override
	public int compare(Carte o1, Carte o2) {
		if((((CardLine) o1).getPop() - ((CardLine) o2).getPop()) > 0) {
			return 1;
		}else {
			if((((CardLine) o1).getPop() - ((CardLine) o2).getPop()) < 0) {
				return -1;
			}else {
				return 0;
			}
		}
	}

}
