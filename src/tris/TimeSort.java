package tris;

import java.util.Comparator;

import jeux.Carte;
import jeux.TimeLine;

public class TimeSort implements Comparator<Carte>{

	@Override
	public int compare(Carte arg0, Carte arg1) {
		// TODO Auto-generated method stub
		return ((TimeLine) arg0).getDate() - ((TimeLine) arg1).getDate();
	}

}
