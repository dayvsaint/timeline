package tris;

import java.util.Comparator;

import jeux.CardLine;
import jeux.Carte;

public class PolSort implements Comparator<Carte>{

	@Override
	public int compare(Carte o1, Carte o2) {
		if((((CardLine) o1).getPol() - ((CardLine) o2).getPol()) > 0) {
			return 1;
		}else {
			if((((CardLine) o1).getPol() - ((CardLine) o2).getPol()) < 0) {
				return -1;
			}else {
				return 0;
			}
		}
	}

}
