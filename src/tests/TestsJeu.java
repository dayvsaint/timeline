package tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import jeux.Jeu;
import jeux.Joueur;
import jeux.PersonnePhysique;

class TestsJeu extends Object {

	
	@BeforeEach
	void before() {
		Jeu.getInstance().getJoueurs().clear();
	}
	
	@Test
	void testAjoutJoueur() throws IOException {
		Jeu.getInstance().ajouterJoueur("Jacques", 3);
		assertTrue("Joueur non ajoute", Jeu.getInstance().getJoueurs().size() == 1);
	}
	
	@Test
	void test8Players() throws IOException {
		for(int i = 0; i < 8; i++) {
			Jeu.getInstance().ajouterJoueur("Jacques"+i, 8 - i);
		}
		assertTrue("Joueurs non ajoutes", Jeu.getInstance().getJoueurs().size() == 8);
	}

	@Test
	void testMoreThan8Players() throws IOException {
		for(int i = 0; i < 10; i++) {
			Jeu.getInstance().ajouterJoueur("Jacques"+i, 10 - i);
		}
		assertTrue("Joueurs non ajoutes", Jeu.getInstance().getJoueurs().size() == 8);
	}
	
	@Test
	void testAjoutBot() throws IOException {
		for(int i = 0; i < 3; i++) {
			Jeu.getInstance().ajouterJoueur("Jacques"+i, 3 - i);
		}
		Jeu.getInstance().completerAvecBots(5);
		assertTrue("Bots non ajoutes", Jeu.getInstance().getJoueurs().size() == 8);
		
	}
	
	@Test
	void distributionMains2ou3Joueurs() throws IOException {
		Jeu.getInstance().remplirJeuTL();
		for(int i = 0; i < 2; i++) {
			Jeu.getInstance().ajouterJoueur("Jacques"+i, 10 - i);
		}
		Jeu.getInstance().distribuerToutesLesMains();
		for(Joueur j : Jeu.getInstance().getJoueurs()) {
			assertTrue("Cartes mal distribuees 2 personnes", ((PersonnePhysique) j).getJeu().size() == 6);
		}
		Jeu.getInstance().getJoueurs().clear();
		for(int i = 0; i < 3; i++) {
			Jeu.getInstance().ajouterJoueur("Jacques"+i, 10 - i);
		}
		Jeu.getInstance().distribuerToutesLesMains();
		for(Joueur j : Jeu.getInstance().getJoueurs()) {
			assertTrue("Cartes mal distribuees 3 personnes", ((PersonnePhysique) j).getJeu().size() == 6);
		}
	}
	
	@Test
	void distributionMains4ou5Joueurs() throws IOException {
		Jeu.getInstance().remplirJeuTL();
		for(int i = 0; i < 4; i++) {
			Jeu.getInstance().ajouterJoueur("Jacques"+i, 10 - i);
		}
		Jeu.getInstance().distribuerToutesLesMains();
		for(Joueur j : Jeu.getInstance().getJoueurs()) {
			assertTrue("Cartes mal distribuees 4 personnes", ((PersonnePhysique) j).getJeu().size() == 5);
		}
		Jeu.getInstance().getJoueurs().clear();
		for(int i = 0; i < 5; i++) {
			Jeu.getInstance().ajouterJoueur("Jacques"+i, 10 - i);
		}
		Jeu.getInstance().distribuerToutesLesMains();
		for(Joueur j : Jeu.getInstance().getJoueurs()) {
			assertTrue("Cartes mal distribuees 5 personnes", ((PersonnePhysique) j).getJeu().size() == 5);
		}
	}
	
	@Test
	void distributionMains6a8Joueurs() throws IOException {
		Jeu.getInstance().remplirJeuTL();
		for(int i = 0; i < 6; i++) {
			Jeu.getInstance().ajouterJoueur("Jacques"+i, 10 - i);
		}
		Jeu.getInstance().distribuerToutesLesMains();
		for(Joueur j : Jeu.getInstance().getJoueurs()) {
			assertTrue("Cartes mal distribuees 6 personnes", ((PersonnePhysique) j).getJeu().size() == 4);
		}
		Jeu.getInstance().getJoueurs().clear();
		for(int i = 0; i < 8; i++) {
			Jeu.getInstance().ajouterJoueur("Jacques"+i, 10 - i);
		}
		Jeu.getInstance().distribuerToutesLesMains();
		for(Joueur j : Jeu.getInstance().getJoueurs()) {
			assertTrue("Cartes mal distribuees 8 personnes", ((PersonnePhysique) j).getJeu().size() == 4);
		}
	}
	
}
