package tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import jeux.CardLine;
import jeux.TimeLine;
import tris.PibSort;
import tris.PolSort;
import tris.PopSort;
import tris.SupSort;
import tris.TimeSort;

class TestsSort {

	TimeLine carteTL1;
	TimeLine carteTL2;
	TimeLine carteTL3;
	
	CardLine carteCL1;
	CardLine carteCL2;
	CardLine carteCL3;
	CardLine carteCL4;
	CardLine carteCL5;
	
	@BeforeEach
	void before() {
		//VALEURS FICTIVES
		carteTL1 = new TimeLine("Découverte du feu", "Feu", -4000);
		carteTL2 = new TimeLine("le caillou fort", "caillou", 2002);
		carteTL3 = new TimeLine("La charrue avant les boeufs", "Charrue", 500);
		
		carteCL1 = new CardLine("France", "france", 60000.2, 66.7, 1300.50, 66.7);
		carteCL2 = new CardLine("Espagne", "espagne", 55419.6, 45.2, 1400.50, 90.7);
		carteCL3 = new CardLine("Italie", "italie", 45026.8, 70.4, 900.50, 78.2);
		carteCL4 = new CardLine("Pologne", "pologne", 30465.6, 21.5, 1200.40, 258.3);
		carteCL5 = new CardLine("Allemagne", "allemagne", 58045.9, 37.2, 1135.20, 125.7);
	}
	
	@Test
	void testTime() {
		ArrayList<TimeLine> okList = new ArrayList<>();
		okList.add(carteTL1);
		okList.add(carteTL3);
		okList.add(carteTL2);
		
		ArrayList<TimeLine> aTest = new ArrayList<>();
		aTest.add(carteTL1);
		aTest.add(carteTL2);
		aTest.add(carteTL3);
		
		assertTrue("Listes egales",!okList.equals(aTest));
		
		Collections.sort(aTest, new TimeSort());
		
		assertTrue("Listes pas egales", okList.equals(aTest));
	}
	
	@Test
	void testCardSup() {
		ArrayList<CardLine> okList = new ArrayList<>();
		okList.add(carteCL4);
		okList.add(carteCL3);
		okList.add(carteCL2);
		okList.add(carteCL5);
		okList.add(carteCL1);
		
		ArrayList<CardLine> aTest = new ArrayList<>();
		aTest.add(carteCL1);
		aTest.add(carteCL2);
		aTest.add(carteCL3);
		aTest.add(carteCL4);
		aTest.add(carteCL5);
		
		assertTrue("Listes egales",!okList.equals(aTest));
		
		Collections.sort(aTest, new SupSort());
		assertTrue("Listes pas egales",okList.equals(aTest));
	}
	
	@Test
	void testCardPop() {
		ArrayList<CardLine> okList = new ArrayList<>();
		okList.add(carteCL4);
		okList.add(carteCL5);
		okList.add(carteCL2);
		okList.add(carteCL1);
		okList.add(carteCL3);
		
		ArrayList<CardLine> aTest = new ArrayList<>();
		aTest.add(carteCL1);
		aTest.add(carteCL2);
		aTest.add(carteCL3);
		aTest.add(carteCL4);
		aTest.add(carteCL5);
		
		assertTrue("Listes egales",!okList.equals(aTest));
		
		Collections.sort(aTest, new PopSort());
		assertTrue("Listes pas egales",okList.equals(aTest));
	}
	
	@Test
	void testCardPIB() {
		ArrayList<CardLine> okList = new ArrayList<>();
		okList.add(carteCL3);
		okList.add(carteCL5);
		okList.add(carteCL4);
		okList.add(carteCL1);
		okList.add(carteCL2);
		
		ArrayList<CardLine> aTest = new ArrayList<>();
		aTest.add(carteCL1);
		aTest.add(carteCL2);
		aTest.add(carteCL3);
		aTest.add(carteCL4);
		aTest.add(carteCL5);
		
		assertTrue("Listes egales",!okList.equals(aTest));
		
		Collections.sort(aTest, new PibSort());
		assertTrue("Listes pas egales",okList.equals(aTest));
	}
	
	@Test
	void testCardPol() {
		ArrayList<CardLine> okList = new ArrayList<>();
		okList.add(carteCL1);
		okList.add(carteCL3);
		okList.add(carteCL2);
		okList.add(carteCL5);
		okList.add(carteCL4);
		
		ArrayList<CardLine> aTest = new ArrayList<>();
		aTest.add(carteCL1);
		aTest.add(carteCL2);
		aTest.add(carteCL3);
		aTest.add(carteCL4);
		aTest.add(carteCL5);
		
		assertTrue("Listes egales",!okList.equals(aTest));
		
		Collections.sort(aTest, new PolSort());
		assertTrue("Listes pas egales",okList.equals(aTest));
	}

}
