package jeux;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class TimeLine extends Carte implements Comparable, Serializable{
	
	public final static String IMAGES_PATH = Main.PROJECT_PATH+File.separator+"data"+File.separator+"timeline"+File.separator+"cards";
	public final static String VERSO_EXTENSION = "_date";

	private int date;
	
	public TimeLine(String v, String img, int dt) {
		super(v, IMAGES_PATH, img, VERSO_EXTENSION);
		date = dt;
	}

	@Override
	public int compareTo(Object obj) {
		int res = 0;
		if(this.date < ((TimeLine) obj).getDate())
			res = -1;
		if(this.date > ((TimeLine) obj).getDate())
			res = -1;
		return res;
	}
	
	public int getDate() {
		return this.date;
	}

	@Override
	public Carte clone() {
		return new TimeLine(this.getNom(), this.getImgName(), date);
	}

}
