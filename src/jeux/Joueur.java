package jeux;

import java.util.ArrayList;
import java.util.Random;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

public abstract class Joueur implements Serializable{
	
	public final static int SPRITE_WIDTH = 40;
	public final static int SPRITE_HEIGHT = 40;
	public final static int TEXT_HEIGHT = 10;
	public final static int GAME_HEIGHT = 20;
	
	private String pseudo;
	private int points;
	private int number;
	private ArrayList<Carte> jeu;
	
	private boolean etat;
	
	public Joueur(String psdo, int num) throws IOException {
		etat = true;
		this.pseudo = psdo;
		this.number = num;
		this.points = 0;
		this.jeu = new ArrayList(); 
		Random rand = new Random();
		int n = rand.nextInt(50) + 1;
		File image_file = new File(Main.PROJECT_PATH + File.separator + "data" + File.separator + "sprite" + File.separator + "players" + File.separator +  "default.png");
	}
	
	public abstract void jouer() throws InterruptedException;
	
	public ArrayList<Carte> getJeu() {
		return this.jeu;
	}
	
	public String getPseudo() {
		return this.pseudo;
	}
	
	/**
	 * Permet d'ajouter un point au joueur, appelle lorsque le joueur place bien une carte
	 */
	public void ajouterPoint() {
		this.points++;
	}
	
	public int getPoints() {
		return points;
	}
	
	public boolean getEtat() {
		return etat;
	}
	
	public void setEtat(boolean b) {
		etat = b;
	}
}
