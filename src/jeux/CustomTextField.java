package jeux;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
/**
 * 
 * @author Pas nous
 *
 * Classe r�cup�r�e sur Internet
 * Permet de creer des JTextFields qui n'autorisent que les chiffres
 */

public class CustomTextField extends JTextField
{
    public CustomTextField()
    {
        super();
    }
 
    @Override
    protected Document createDefaultModel()
    {
        return new FileCaseDocument();
    }
 
    static class FileCaseDocument extends PlainDocument
    {
        boolean canInsert = false;
        String text = null;
        String str1, str2;
 
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
        {
 
            if (str == null ||  this.getLength()>=20) // pas plus de 20 caract�res
            {
                return;
            }
 
// pour g�rer les copier-coller ou les insertions de nombres dans le champ
            text = this.getText(0, this.getLength());
            str1 = text.substring(0, offs);
            str2 = text.substring(offs, this.getLength());
 
            text = str1+str+str2;
 
            // on autorise uniquement les entiers positifs et n�gatifs
            canInsert = text.matches( "(-[1-9][0-9]*)|(0)|([1-9][0-9]*)" );
 
           // nombre n�gatifs : on autorise le signe "-"
            if (str.compareTo("-")==0 && this.getLength()==0)
            {
                canInsert = true;
            }
 
            if (canInsert)
            {
                super.insertString(offs, str, a);
            }
         }
    }
}