package jeux;

import java.io.IOException;
import java.io.Serializable;
import java.util.Random;

public class Bot extends Joueur implements Serializable{

	public static int nbBots = 0;
	
	public Bot(String psdo, int num) throws IOException {
		super(psdo, num);
		nbBots++;
	}
	
	/**
	 * En fonction de la difficulte
	 * Le bot choisira la premiere carte de sa main (liste)
	 * Si facile = 50% de chance de bien la placer 
	 * Si difficile 90% de chance de bien placer
	 * Si bon, joueur suivant
	 * Sinon pioche puis joueur suivant
	 * @throws InterruptedException 
	 */
	public void jouer() throws InterruptedException {
		Random r = new Random();
		double chance = r.nextInt(100);
		boolean reussite = false;
		if(Jeu.getInstance().getDiff()) {
			if(chance > 50) {
			reussite = true;	
			}
		}else {
			if(chance < 90) {
				reussite = true;
			}
		}
		if(reussite) {
			Jeu.getInstance().getLine().add(this.getJeu().get(0));
			Jeu.getInstance().sortByCritera();
			this.ajouterPoint();
			this.getJeu().remove(0);
		} else {
			this.getJeu().remove(0);
			Jeu.getInstance().distribuerCarte(this);
		}
	}

}
