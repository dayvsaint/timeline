package jeux;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

public abstract class Carte implements Cloneable, Serializable{
	
	private String nom, img, recto_path, verso_path;
	private transient BufferedImage recto, verso;
	
	public Carte(String n, String image_path, String img, String verso_extension) {
		this.img = img;
		this.nom = n;
		this.recto_path = image_path + File.separator + img + ".jpeg";
		this.verso_path = image_path + File.separator + img + verso_extension + ".jpeg";
	}
	
	public String getNom() {
		return nom;
	}
	
	public String getImgName() {
		return this.img;
	}
	
	
	public abstract Carte clone();
	
	
	public BufferedImage getRecto() {
		if(this.recto == null) {
			try {
				File recto_image_file = new File(this.recto_path);
				this.recto = ImageIO.read(recto_image_file);				
			} catch (IOException e) {
				System.out.println("Erreur de lecture/ecriture sur les fichiers d'image, verifiez les droits d'acces");
				e.printStackTrace();
			}
		}
		
		return this.recto;
	}
	
	public BufferedImage getVerso() {
		if(this.verso == null) {
			try {
				File verso_image_file = new File(this.verso_path);
				this.verso = ImageIO.read(verso_image_file);
			} catch (IOException e) {
				System.out.println("Erreur de lecture/ecriture sur les fichiers d'image, verifiez les droits d'acces");
				e.printStackTrace();
			}
		}
		
		return this.verso;
	}
}
