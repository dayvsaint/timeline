package jeux;

import java.io.IOException;
import java.io.Serializable;

public class PersonnePhysique extends Joueur implements Serializable{
	
	int age;
	
	public PersonnePhysique(String psdo, int age, int num) throws IOException {
		super(psdo, num);
		this.age = age;
	}
	
	/**
	 * Inutile, gere dans la fentre du main
	 */
	public void jouer() {
		
	}
	
	public int getAge() {
		return this.age;
	}

}
