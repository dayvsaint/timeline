package jeux;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import tris.PibSort;
import tris.PolSort;
import tris.PopSort;
import tris.SupSort;
import tris.TimeSort;

public class Jeu implements Serializable {
	
	public static final int AREA = 0;
	public static final int POPULATION = 1;
	public static final int POLLUTION = 2;
	public static final int PIB = 3;
	
	private static Jeu game;
	
	private ArrayList<Carte> pioche = new ArrayList<>();
	private ArrayList<Carte> line = new ArrayList<>();
	private ArrayList<Joueur> joueurs = new ArrayList<>();
	
	private boolean facile;
	private int critere;
	private Joueur turn_player;
	
	public Jeu() {
		this.pioche = new ArrayList<>();
		this.line = new ArrayList<>();
		this.joueurs = new ArrayList<>();
		this.facile = true;
	}
	
	public static Jeu getInstance() {
		if(Jeu.game == null)
			Jeu.game = new Jeu();
		return Jeu.game;
	}
	
	//meilleur score a sauvegarder, pas implemente
	public String bestScore;
	
	
	/**********************************************
	 **		Methodes de gestion de la pioche	 ** 
	 **********************************************/
	
	/**
	 * Methode permettant de distribuer une carte a un joueur donne
	 * 
	 * @param j, joueur auquel distribuer une carte
	 */
	public void distribuerCarte(Joueur j) {
		Carte c = pioche.get(0);
		ArrayList<Carte> player_card_game = j.getJeu();
		if(player_card_game.size() < 6) {
			player_card_game.add(c.clone());
			pioche.remove(0);
		}
	}
	
	/**
	 * Permet de savoir si un nombre est compris entre deux autres
	 * 
	 * @param x
	 * @param lower
	 * @param upper
	 * @return
	 */
	public boolean isBetween(int x, int lower, int upper) {
	  return lower <= x && x <= upper;
	}
	
	/**
	 * Permet de distribuer leur main de depart a tous les joueurs presents 
	 * en ajustant le nombre par rapport au nombre de joueurs
	 * comme precise dans les regles du jeu
	 */
	public void distribuerToutesLesMains() {
		int nbCarteMain = 6;
		if(isBetween(joueurs.size(),2,3)) {
			nbCarteMain = 6;
		}else {
			if(isBetween(joueurs.size(),4,5)) {
				nbCarteMain = 5;
			}else {
				if(isBetween(joueurs.size(),6,8)) {
					nbCarteMain = 4;
				}
			}
		}
		for(int i = 0; i < nbCarteMain; i++) {
			for(int j = 0; j < joueurs.size(); j++) {
				distribuerCarte(joueurs.get(j));
			}
		}
	}
	
	/**
	 * Methode remettant ou non une carte defaussee par un joueur 
	 * qui s'est trompe en fonction de la difficulte choisie
	 * 
	 * @param c, carte defaussee par le joueur qui s'est trompe
	 */
	public void remettreCarte(Carte c) {
		if(this.facile) {
			this.pioche.add(c.clone());
		}
	}
	
	/**
	 * Permet de regler la difficulter du jeu
	 * @param diff
	 */
	public void setDifficulty(boolean diff) {
		this.facile = diff;
	}
	
	/**********************************************
	 **		Methodes de creation de la pioche	 ** 
	 **********************************************/
	
	/**
	 * Methode creant la pioche du jeu CardLine et la melangeant de maniere aleatoire.
	 */
	public  void remplirJeuCL() {
		this.pioche.clear();
		try {
			FileReader fr = new FileReader(new File("data"+File.separator+"cardline"+File.separator+"cardline.csv"));
			BufferedReader br = new BufferedReader(fr);
			String s = br.readLine();
			
			while((s = br.readLine()) != null) {
				String[] tab = s.split(";");
				this.pioche.add(new CardLine(tab[0], tab[5],
						Float.parseFloat(tab[1].replaceAll(",", ".")),
						Float.parseFloat(tab[2].replaceAll(",", ".")),
						Float.parseFloat(tab[3].replaceAll(",", ".")),
						Float.parseFloat(tab[4].replaceAll(",", "."))));
			}
			
			Collections.shuffle(this.pioche);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Fichier data"+File.separator+"cardline"+File.separator+"cardline.csv introuvable.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Methode creant la pioche du jeu TimeLine et la melangeant de maniere aleatoire.
	 */
	public  void remplirJeuTL() {
		this.pioche.clear();
		try {
			FileReader fr = new FileReader(new File("data"+File.separator+"timeline"+File.separator+"timeline.csv"));
			BufferedReader br = new BufferedReader(fr);
			String s = br.readLine();
			
			while((s = br.readLine()) != null) {
				String[] tab = s.split(";");
				this.pioche.add(new TimeLine(tab[0], tab[2], Integer.parseInt(tab[1])));
			}
			
			Collections.shuffle(this.pioche);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Fichier data"+File.separator+"timeline"+File.separator+"timeline.csv introuvable.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode permettant de changer le critere de victoire d'une partie CardLine
	 */
	public void setCritera(int i) {
		this.critere = i;
	}
	

	/**********************************************
	 **		 Methodes de gestion des joueurs 	 ** 
	 **********************************************/
	
	public ArrayList<Joueur> getJoueurs(){
		return this.joueurs;
	}
	
	/**
	 * Methode permettant d'ajouter un joueur a la partie
	 * Tant que le nombre de joueurs est inferieur a 8, on peut ajouter des joueurs
	 * Defini le joueur le plus jeune pour le debut de partie
	 * 
	 * @param j, joueur a ajouter
	 * @throws IOException 
	 */
	
	public void ajouterJoueur(String psdo, int a) throws IOException {
		if(this.joueurs.size() < 8) {
			PersonnePhysique new_player = new PersonnePhysique(psdo, a, joueurs.size());
			this.joueurs.add(new_player);
			if(turn_player == null || ((PersonnePhysique)turn_player).getAge() > a) 
				turn_player = new_player;
		}else {
			System.out.println("Il ne peut y avoir plus de 8 joueurs.");
		}
	}
	
	/**
	 * Met a jour le joueur dont c'est le tour de jouer
	 */
	public void incrementerTurnPlayer() {
		int i = this.joueurs.indexOf(turn_player)+1;
		if(i == this.joueurs.size()) {
			i = 0;
		}
		turn_player = joueurs.get(i);
	}

	/**
	 * Complete la liste des joueurs avec le nombre de bots indiques
	 * 
	 * @throws IOException 
	 * 
	 */
	
	public void completerAvecBots(int nb) throws IOException {
		for(int i = 0; i < nb; i++) {
			this.joueurs.add(new Bot("BOT" + this.joueurs.size(), this.joueurs.size()));
		}
		if(turn_player == null) {
			turn_player = this.joueurs.get(0);
		}
	}
	
	/**
	 * Initialise la line de depart en prenant la premiere carte du paquet
	 */
	public void debutLine() {
		line.clear();
		Carte c = pioche.get(0);
		line.add(c.clone());
		pioche.remove(0);
	}

	public Joueur getTurnPlayer() {
		return turn_player;
	}

	public ArrayList<Carte> getLine() {
		return line;
	}
	
	public boolean getDiff() {
		return facile;
	}
	
	public int getCritere() {
		return critere;
	}
	
	/**
	 * Retourne la valeur souhaite de la carte d'index i en fonction du critere du jeu
	 * 
	 * @param i
	 * @return
	 */
	public double getValByCritera(int i) {
		double val = 0;
		if(line.get(i) instanceof TimeLine) {
			TimeLine tl = (TimeLine) line.get(i);
			val = tl.getDate();
		}else {
			CardLine cl = (CardLine)  line.get(i);
			switch(this.critere) {
			case(Jeu.AREA):
				val = cl.getSup();
				break;
			case(Jeu.POPULATION):
				val = cl.getPop();
				break;
			case(Jeu.POLLUTION):
				val = cl.getPol();
				break;
			case(Jeu.PIB):
				val = cl.getPIB();
				break;
			}
		}
		return val;
	}
	
	/**
	 * Range la line de jeu en fonction du critere du jeu
	 */
	public void sortByCritera() {
		if(line.get(0) instanceof TimeLine) {
			Collections.sort(line, new TimeSort());
		}else {
			switch(this.critere) {
			case(Jeu.AREA):
				Collections.sort(line, new SupSort());
				break;
			case(Jeu.POPULATION):
				Collections.sort(line, new PopSort());
				break;
			case(Jeu.POLLUTION):
				Collections.sort(line, new PolSort());
				break;
			case(Jeu.PIB):
				Collections.sort(line, new PibSort());
				break;
			}
		}
	}
	
	/**
	 * Methode qui permet de sauvegarder une partie
	 */
	public void save(String file_path) {
		File file =  new File(file_path) ;
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file)) ;
			oos.writeObject(Jeu.getInstance()) ;
			System.out.println(Arrays.toString(this.line.toArray()));
		} catch (FileNotFoundException e) {
			System.out.println("Fichier non trouvé");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Problème lecture/écriture");
			e.printStackTrace();
		}
	}
	
	/**
	 * Methode qui permet de charger une partie
	 */
	public static void load(String file_path) {
		File file = new File(file_path) ;
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file)) ;
			Jeu.game = (Jeu)ois.readObject() ;
			System.out.println(Arrays.toString(Jeu.getInstance().line.toArray()));
		} catch (FileNotFoundException e) {
			System.out.println("Fichier non trouvé");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Problème lecture/écriture");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
