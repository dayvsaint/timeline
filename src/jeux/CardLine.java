package jeux;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class CardLine extends Carte implements Serializable{

	private double superficie, population, pib, pollution;
	
	public final static String IMAGES_PATH = Main.PROJECT_PATH+File.separator+"data"+File.separator+"cardline"+File.separator+"cards";
	public final static String VERSO_EXTENSION = "_reponse";
			
	public CardLine(String v, String img, double s, double pop, double pi, double pol) {
		super(v, IMAGES_PATH, img, VERSO_EXTENSION);
		this.superficie = s;
		this.population = pop;
		this.pib = pi;
		this.pollution = pol;
	}
	
	public double getSup() {
		return superficie;
	}
	
	public double getPop() {
		return population;
	}
	
	public double getPIB() {
		return pib;
	}
	
	public double getPol() {
		return pollution;
	}

	@Override
	public Carte clone() {
		return new CardLine(this.getNom(), this.getImgName(), this.superficie, this.population, this.pib, this.pollution);
	}
}
