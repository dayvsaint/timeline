package jeux;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Main {
	
	public final static String PROJECT_PATH = System.getProperty("user.dir");
	
	public static void main(String[] args) {	
		createMenu();
	}
	
	/**
	 * Cree le menu de depart de l'application avec pour choix le mode de jeu, nouvelle partie ou charger une partie (pas implemente)
	 */
	public static void createMenu() {
		JFrame menu = new JFrame("TimeLine/CardLine");
		String[] choix = {"TimeLine", "CardLine"};
		JComboBox<String> jcb = new JComboBox<String>(choix);
		JButton newGame = new JButton("Nouvelle partie");
		JButton loadGame = new JButton("Charger une partie");
		jcb.setAlignmentX(Component.CENTER_ALIGNMENT);
		newGame.setAlignmentX(Component.CENTER_ALIGNMENT);
		loadGame.setAlignmentX(Component.CENTER_ALIGNMENT);
		jcb.setPreferredSize(new Dimension(390, 100));
		
		newGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				switch(jcb.getSelectedIndex()) {
				case(0):
					Jeu.getInstance().remplirJeuTL();
					createCharactersMenu(0);
					break;
				case(1):
					Jeu.getInstance().remplirJeuCL();
					createCharactersMenu(1);
					break;
				}
				menu.setVisible(false);
			}
			
		});
		
		loadGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
			    int returnVal = chooser.showOpenDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			       System.out.println("You chose to open this file: " +
			            chooser.getSelectedFile().getAbsolutePath());
			       		Jeu.load(chooser.getSelectedFile().getAbsolutePath());
			       		int jeu;
			       		if(Jeu.getInstance().getLine().get(0) instanceof TimeLine)
			       			jeu = 0;
			       		else 
			       			jeu = 1;
			       		displayGame(jeu,false);
			    }
			}
			
		});
		
		JPanel jpMenu = new JPanel();
		jpMenu.setLayout(new BoxLayout(jpMenu, BoxLayout.Y_AXIS));
		jpMenu.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		jpMenu.add(jcb);
		jpMenu.add(Box.createRigidArea(new Dimension(0, 5)));
		jpMenu.add(newGame);
		jpMenu.add(Box.createRigidArea(new Dimension(0, 5)));
		jpMenu.add(loadGame);		
		jpMenu.add(Box.createRigidArea(new Dimension(0, 5)));
		
		menu.add(jpMenu);
		menu.setSize(400, 150);
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu.setVisible(true);
		menu.setResizable(false);
		menu.setLocationRelativeTo(null);
	}
	
	/**
	 * Cree la fenetre pour choisir la difficulte, le nombre de joueur, leur nom, leur age, 
	 * le nombre de bots et le critere de victoire si le jeu est cardline
	 * 
	 * @param jeu, type du jeu Timeline ou Cardline
	 */
	public static void createCharactersMenu(int jeu) {
		
		JPanel jpAll = new JPanel();
		jpAll.setLayout(new BoxLayout(jpAll, BoxLayout.Y_AXIS));
		
		//TITRE
		String s = "";
		if(jeu == 0) {
			s = "TimeLine";
		}else {
			s = "CardLine";
		}
		JFrame characters = new JFrame(s);
		
		JPanel jpCharNord = new JPanel();
		jpCharNord.setLayout(new BoxLayout(jpCharNord, BoxLayout.Y_AXIS));
		
		JLabel titre = new JLabel(s, JLabel.CENTER);
		titre.setFont(new Font(titre.getFont().getName(), Font.BOLD, 30));
		titre.setAlignmentX(Component.CENTER_ALIGNMENT);
		jpCharNord.add(titre);
		jpCharNord.setMaximumSize(new Dimension(400, 35));
		
		//JOUEURS
		JPanel jpCharCenter = new JPanel();
		jpCharCenter.setLayout(new BoxLayout(jpCharCenter, BoxLayout.Y_AXIS));
		
		
		JPanel botsjp = new JPanel();
		botsjp.setLayout(new BoxLayout(botsjp, BoxLayout.X_AXIS));
		JLabel botsLab = new JLabel("Ajouter des bots : ");
		botsLab.setAlignmentX(Component.CENTER_ALIGNMENT);
		botsjp.add(botsLab);
		JCheckBox botsjcb = new JCheckBox();
		botsjcb.setSelected(false);
		botsjcb.setAlignmentX(Component.CENTER_ALIGNMENT);
		botsjp.add(botsjcb);
		JComboBox<Integer> botsjco = new JComboBox<>();
		for(int c = 1; c <= 8; c++) {
			botsjco.addItem(c);
		}
		botsjcb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(botsjcb.isSelected()) {
					botsjco.setEnabled(true);
				}else {
					botsjco.setEnabled(false);
				}
			}
			
		});
		botsjco.setAlignmentX(Component.CENTER_ALIGNMENT);
		botsjco.setMaximumSize(new Dimension(40,20));
		botsjp.add(botsjco);
		
		JButton plus = new JButton("Ajouter un joueur");
		plus.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		ArrayList<JTextField> jtfList = new ArrayList<>();
		ArrayList<CustomTextField> jcbList = new ArrayList<>();
		ArrayList<JButton> moinsList = new ArrayList<>();
		ArrayList<JPanel> jpList = new ArrayList<>();
		
		plus.addActionListener(new ActionListener() {
		int compteur = 0;
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				JPanel jpPers = new JPanel();
				jpPers.setMaximumSize(new Dimension(400, 30));
				jpPers.setLayout(new BoxLayout(jpPers, BoxLayout.X_AXIS));
				
				JTextField tfpsdo = new JTextField(15);
				tfpsdo.setAlignmentX(Component.CENTER_ALIGNMENT);
				CustomTextField jcbAge = new CustomTextField();
				jcbAge.setAlignmentX(Component.CENTER_ALIGNMENT);
				
				JButton moins = new JButton("-");
				moins.setAlignmentX(Component.CENTER_ALIGNMENT);
				moins.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						int index = moinsList.indexOf(arg0.getSource());
						
						jpPers.remove(jtfList.get(index));
						jtfList.remove(index);
						jpPers.remove(jcbList.get(index));
						jcbList.remove(index);
						jpPers.remove(moinsList.get(index));
						moinsList.remove(index);
						jpCharCenter.remove(jpList.get(index));
						jpList.remove(index);
						
						characters.pack();
						if(jeu > 0) {
							characters.setSize(400, 600);
						}else {
							characters.setSize(400, 450);
						}
						plus.setEnabled(true);
						compteur--;
						botsjco.removeAllItems();
						botsjco.setEnabled(true);
						botsjcb.setEnabled(true);
						if(compteur < 8) {
							for(int j = 1; j <= (8 - compteur); j++) {
								botsjco.addItem(j);
							}
						}else {
							botsjco.setEnabled(false);
							botsjcb.setEnabled(false);
						}
						
						characters.repaint();
					}
					
				});
				compteur++;
				jpCharCenter.add(jpPers);
				jpPers.add(tfpsdo);
				jpPers.add(jcbAge);
				jpPers.add(moins);
				
				jtfList.add(tfpsdo);
				jcbList.add(jcbAge);
				moinsList.add(moins);
				jpList.add(jpPers);
				
				botsjco.removeAllItems();
				botsjco.setEnabled(true);
				botsjcb.setEnabled(true);
				if(compteur < 8) {
					for(int j = 1; j <= (8 - compteur); j++) {
						botsjco.addItem(j);
					}
				}else {
					botsjco.setEnabled(false);
					botsjcb.setEnabled(false);
				}
								
				if(compteur >= 8) {
					plus.setEnabled(false);
					plus.setToolTipText("Il ne peut y avoir plus de 8 joueurs");
				}else {
					plus.setEnabled(true);
					plus.setToolTipText(null);
				}
				characters.pack();
				if(jeu > 0) {
					characters.setSize(400, 600);
				}else {
					characters.setSize(400, 450);
				}
				characters.repaint();
			}
			
		});
		
		
		
		jpCharCenter.add(botsjp);
		jpCharCenter.add(plus);
		jpCharCenter.setMaximumSize(new Dimension(400,300));
				
		//Fusion 1 et 2
		jpAll.add(jpCharNord);
		JSeparator js1 = new JSeparator(SwingConstants.HORIZONTAL);
		js1.setMaximumSize(new Dimension(400,5));
		jpAll.add(js1);
		jpAll.add(jpCharCenter);
		JSeparator js2 = new JSeparator(SwingConstants.HORIZONTAL);
		js2.setMaximumSize(new Dimension(400,5));
		jpAll.add(js2);
		
		//SI CardLine, Critere
		ButtonGroup bg = new ButtonGroup();
		JRadioButton sup = new JRadioButton("Superficie");
		sup.setSelected(true);
		JRadioButton pop = new JRadioButton("Population");
		JRadioButton pol = new JRadioButton("Pollution");
		JRadioButton pib = new JRadioButton("PIB");
		bg.add(sup);
		bg.add(pop);
		bg.add(pol);
		bg.add(pib);
		
				if(jeu > 0) {
					JPanel jpCrit = new JPanel();
					jpCrit.setLayout(new BoxLayout(jpCrit, BoxLayout.Y_AXIS));
					
					JLabel crit = new JLabel("Critere de victoire", JLabel.CENTER);
					crit.setFont(new Font(crit.getFont().getName(), Font.PLAIN, 15));
					crit.setAlignmentX(Component.CENTER_ALIGNMENT);
					jpCrit.add(crit);
										
					JPanel crit1 = new JPanel();
					crit1.setLayout(new BoxLayout(crit1, BoxLayout.X_AXIS));
					JPanel crit2 = new JPanel();
					crit2.setLayout(new BoxLayout(crit2, BoxLayout.X_AXIS));
					crit1.add(sup);
					crit1.add(pop);
					crit2.add(pol);
					crit2.add(pib);
					
					jpCrit.add(crit1);
					jpCrit.add(crit2);
					
					jpCrit.setMaximumSize(new Dimension(400, 70));
					jpAll.add(jpCrit);
					JSeparator js3 = new JSeparator(SwingConstants.HORIZONTAL);
					js3.setMaximumSize(new Dimension(400,5));
					jpAll.add(js3);
				}
		
		//DIFFICULTE
		JPanel jpDiff = new JPanel();
		jpDiff.setLayout(new BoxLayout(jpDiff, BoxLayout.Y_AXIS));
				
		JLabel diff = new JLabel("Difficulte", JLabel.CENTER);
		diff.setFont(new Font(diff.getFont().getName(), Font.PLAIN, 15));
		diff.setAlignmentX(Component.CENTER_ALIGNMENT);
		jpDiff.add(diff);	
		
		ButtonGroup bgDiff = new ButtonGroup();
		JRadioButton facile = new JRadioButton("Facile");
		facile.setSelected(true);
		JRadioButton difficile = new JRadioButton("Difficile");
		bgDiff.add(facile);
		bgDiff.add(difficile);
		
		JPanel jpDiff1 = new JPanel();
		jpDiff1.setLayout(new BoxLayout(jpDiff1, BoxLayout.X_AXIS));
		jpDiff1.add(facile);
		jpDiff1.add(difficile);
		jpDiff.add(jpDiff1);
		
		jpDiff.setMaximumSize(new Dimension(400, 50));
		jpAll.add(jpDiff);
		JSeparator js4 = new JSeparator(SwingConstants.HORIZONTAL);
		js4.setMaximumSize(new Dimension(400,5));
		jpAll.add(js4);
		
		//REGLES LANCER
		JPanel jpBas = new JPanel();
		jpBas.setLayout(new BoxLayout(jpBas, BoxLayout.X_AXIS));
		
		JButton lancer = new JButton("Lancer");
		JButton regles = new JButton("Regles");
		
		lancer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean ok = true;
				
				if(!jcbList.isEmpty()) {
					for(int players = 0; players < jtfList.size(); players++) {
						if(jcbList.get(players).getText().isEmpty()) {
							ok = false;
						}
						if(jtfList.get(players).getText().isEmpty()) {
							ok = false;
						}
					}
				}
				
				if(ok) {
					//Ajouter les joueurs
					for(int players = 0; players < jtfList.size(); players++) {
						try {
							int age = 0;
							if(!(jcbList.get(players).getText().isEmpty())) {
								age = Integer.parseInt(jcbList.get(players).getText());
							}
							Jeu.getInstance().ajouterJoueur(jtfList.get(players).getText(), age);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					//Ajouter les bots
					
					if(Jeu.getInstance().getJoueurs().size() == 1 && !botsjcb.isSelected()) {
						try {
							Jeu.getInstance().completerAvecBots(1);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else {
						if(Jeu.getInstance().getJoueurs().size() == 0 && !botsjcb.isSelected()) {
							try {
								Jeu.getInstance().completerAvecBots(2);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}else {
							if(botsjcb.isSelected()) {
								try {
									Jeu.getInstance().completerAvecBots(botsjco.getItemAt(botsjco.getSelectedIndex()));
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}
					//Creer les mains
					Jeu.getInstance().distribuerToutesLesMains();
					//Mettre le critere
					if(sup.isSelected()) {
						Jeu.getInstance().setCritera(0);
					}else {
						if(pop.isSelected()) {
							Jeu.getInstance().setCritera(1);
						}else {
							if(pol.isSelected()) {
								Jeu.getInstance().setCritera(2);
							}else {
								Jeu.getInstance().setCritera(3);
							}
						}
					}
					//set difficulte
					if(facile.isSelected()) {
						Jeu.getInstance().setDifficulty(true);
					}else {
						Jeu.getInstance().setDifficulty(false);
					}
					
					//Lancer l'autre fenetre de jeu
					displayGame(jeu, true);
					characters.setVisible(false);
				
				}
				
			}		
		});
		
		regles.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(jeu > 0) {
					try {
						Desktop.getDesktop().browse(new URL("http://jeuxstrategie1.free.fr/jeu_cardline/regle.pdf").toURI());
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (URISyntaxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else {
					try {
						Desktop.getDesktop().browse(new URL("http://jeuxstrategie1.free.fr/jeu_timeline/regle.pdf").toURI());
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (URISyntaxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
			
		});
		jpBas.add(lancer);
		jpBas.add(regles);
		jpAll.add(jpBas);
		//FIN
		characters.add(jpAll);
		if(jeu > 0) {
			characters.setSize(400, 600);
		}else {
			characters.setSize(400, 450);
		}
		characters.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		characters.setVisible(true);
		characters.setResizable(false);
		characters.setLocationRelativeTo(null);
	}
	
	/**
	 * Fenetre du jeu, contient les scores, les joueurs et leur nombre de cartes, si Cardline contient des radiobutton pour changer le critere de victoire
	 * Contient la main du joueur dont c'est le tour de jouer, la ligne de cartes rangee, la fenetre de dialogue, des jtextfields ou rentrer
	 * la place de la carte a placer et la place dans la ligne ou la placer et une fenetre pour afficher la carte de la main souhaite en plus grand
	 * 
	 * @param jeu, type du jeu
	 */
	public static void displayGame(int jeu, boolean nouveau) {

		boolean fin = false;
		
		if(nouveau) {
			Jeu.getInstance().debutLine();	
		}
		String titre;
		if(jeu > 0) {
			titre = "CardLine";
		}else {
			titre = "TimeLine";
		}
		JFrame game = new JFrame(titre);
		
		
		//Gauche - Scores
		JPanel scores = new JPanel();
		scores.setLayout(new BoxLayout(scores, BoxLayout.Y_AXIS));
		scores.setMaximumSize(new Dimension(188,433));
		scores.setMinimumSize(new Dimension(188,433));
		JTextArea jtaS = new JTextArea();
		jtaS.setEditable(false);
		for(Joueur j : Jeu.getInstance().getJoueurs()) {
			if(j.getEtat()) {
				jtaS.setText(jtaS.getText() + j.getPseudo() + " : " + j.getPoints() + "\n");
			}else {
				jtaS.setText(jtaS.getText() + "*PERDU*" + j.getPseudo() + " : " + j.getPoints() + "\n");
			}
		}
		scores.add(jtaS);
		
		
		//Gauche - Boutons
		JPanel boutons = new JPanel();
		boutons.setMaximumSize(new Dimension(188,133));
		boutons.setMinimumSize(new Dimension(188,133));
		boutons.setLayout(new BoxLayout(boutons, BoxLayout.Y_AXIS));
		JButton save = new JButton("Sauvegarder");
		save.setAlignmentX(Component.CENTER_ALIGNMENT);
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
			    int returnVal = chooser.showOpenDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			       System.out.println("You chose to open this file: " +
			            chooser.getSelectedFile().getAbsolutePath());
			       		Jeu.getInstance().save(chooser.getSelectedFile().getAbsolutePath());
			    }
			}
			
		});
		boutons.add(save);
			//changement de critere gauche
		ButtonGroup bg = new ButtonGroup();
		JRadioButton sup = new JRadioButton("Superficie");
		JRadioButton pop = new JRadioButton("Population");
		JRadioButton pol = new JRadioButton("Pollution");
		JRadioButton pib = new JRadioButton("PIB");
		

		bg.add(sup);
		bg.add(pop);
		bg.add(pol);
		bg.add(pib);
		
		switch(Jeu.getInstance().getCritere()) {
		case(0):
			sup.setSelected(true);
			break;
		case(1):
			pop.setSelected(true);
			break;
		case(2):
			pol.setSelected(true);
			break;
		case(3):
			pib.setSelected(true);
			break;
		}	
		JPanel crit1 = new JPanel();
		crit1.setLayout(new BoxLayout(crit1, BoxLayout.X_AXIS));
		JPanel crit2 = new JPanel();
		crit2.setLayout(new BoxLayout(crit2, BoxLayout.X_AXIS));
		crit1.add(sup);
		crit1.add(pop);
		crit2.add(pol);
		crit2.add(pib);
		
		if(!(jeu > 0)) {
			sup.setEnabled(false);
			pop.setEnabled(false);
			pol.setEnabled(false);
			pib.setEnabled(false);
		}
		
		boutons.add(crit1);
		boutons.add(crit2);
		
		//Gauche - Fusion
		boutons.setAlignmentX(Component.CENTER_ALIGNMENT);
		boutons.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		scores.setAlignmentX(Component.CENTER_ALIGNMENT);
		scores.setBackground(Color.WHITE);
		scores.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		JPanel gauche = new JPanel();
		gauche.setLayout(new BoxLayout(gauche, BoxLayout.Y_AXIS));
		gauche.add(scores);
		JSeparator js1 = new JSeparator(SwingConstants.HORIZONTAL);
		js1.setMaximumSize(new Dimension(180,10));
		gauche.add(js1);
		gauche.add(boutons);
		gauche.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		gauche.setPreferredSize(new Dimension(190, 590));
		
		//Droite - Zoom
		
		JPanel zoom = new JPanel();
		zoom.setMaximumSize(new Dimension(283,333));
		zoom.setMinimumSize(new Dimension(283,333));
		zoom.setPreferredSize(new Dimension(283,333));
		zoom.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
		//afficher la carte sur laquelle on clique mais pas ici
		
		
		//Droite - Dialogue
		JPanel dialogue = new JPanel();
		dialogue.setLayout(new BoxLayout(dialogue, BoxLayout.Y_AXIS));
		dialogue.setMaximumSize(new Dimension(283,233));
		dialogue.setMinimumSize(new Dimension(283,233));
		dialogue.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		JTextArea jta = new JTextArea();
		jta.setEditable(false);
		jta.setText("Debut de la partie...");
		jta.setText(jta.getText() + "\nAu tour de " + Jeu.getInstance().getTurnPlayer().getPseudo() + " de jouer.");
		if(Jeu.getInstance().getTurnPlayer() instanceof PersonnePhysique) {
			jta.setText(jta.getText() + "\nEntrez la place de la carte de la main (0 pour la premiere) puis" +
					"\nEntrez la place ou inserer votre carte \n"
					+ "0 pour la placer en premiere position.");
		}else {
			jta.setText(jta.getText() + "\nLe joueur est un bot, appuyez sur OK");
		}
		JScrollPane jsp = new JScrollPane(jta);
		jsp.setPreferredSize(new Dimension(283,200));
		jsp.setAlignmentX(Component.CENTER_ALIGNMENT);
		dialogue.add(jsp);
		
		JPanel reponse = new JPanel();
		reponse.setLayout(new BoxLayout(reponse, BoxLayout.X_AXIS));
        
		NumberFormat amountFormat = NumberFormat.getNumberInstance();
		
		CustomTextField jcbMain =  new CustomTextField();
		jcbMain.setAlignmentX(Component.CENTER_ALIGNMENT);
		reponse.add(jcbMain);
		
		CustomTextField jcbLine =  new CustomTextField();
		jcbLine.setAlignmentX(Component.CENTER_ALIGNMENT);
		reponse.add(jcbLine);
		
		JButton ok = new JButton("OK");
		reponse.add(ok);
		dialogue.add(reponse);
		
		//Droite - Fusion
		zoom.setAlignmentX(Component.CENTER_ALIGNMENT);
		zoom.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Color.BLACK));
		dialogue.setAlignmentX(Component.CENTER_ALIGNMENT);
		dialogue.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		JPanel droite = new JPanel();
		droite.setLayout(new BoxLayout(droite, BoxLayout.Y_AXIS));
		droite.add(zoom);
		JSeparator js2 = new JSeparator(SwingConstants.HORIZONTAL);
		js2.setMaximumSize(new Dimension(280,10));
		gauche.add(js2);
		droite.add(dialogue);
		droite.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		droite.setPreferredSize(new Dimension(290, 590));
		
		
		//Centre - Joueurs
		JPanel haut = new JPanel();
		haut.setAlignmentX(Component.CENTER_ALIGNMENT);
		haut.setMaximumSize(new Dimension(988,88));
		haut.setMinimumSize(new Dimension(988,88));
		haut.setPreferredSize(new Dimension(988,88));
		haut.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
		haut.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Color.BLACK));
		JTextArea jtaJ = new JTextArea();
		jtaJ.setEditable(false);
		for(Joueur j : Jeu.getInstance().getJoueurs()) {
			jtaJ.setText(jtaJ.getText() + j.getPseudo() + " : " + j.getJeu().size() + " cartes restantes\t");
		}
		haut.add(jtaJ);
		haut.setBackground(Color.white);
		
		//Afficher les joueurs dedans
		
		//Centre - Jeu
		JPanel partie = new JPanel();
		partie.setAlignmentX(Component.CENTER_ALIGNMENT);
		partie.setMinimumSize(new Dimension(988,338));
		partie.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		partie.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));

		JScrollPane jspJeu = new JScrollPane(partie);
		jsp.setPreferredSize(new Dimension(988,338));
		for(Carte c : Jeu.getInstance().getLine()) {
			ImageIcon ii = new ImageIcon(c.getVerso());
			ImageIcon adj = new ImageIcon(ii.getImage().getScaledInstance(220, 300, Image.SCALE_SMOOTH));
			JLabel img = new JLabel(adj);
			partie.add(img);
		}
		
		//Centre - Main
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.X_AXIS));
		main.setAlignmentX(Component.CENTER_ALIGNMENT);
		main.setMaximumSize(new Dimension(988,128));
		main.setMinimumSize(new Dimension(988,128));
		main.setPreferredSize(new Dimension(988,128));
		main.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		main.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, Color.BLACK));
		
		
		for(Carte c : Jeu.getInstance().getTurnPlayer().getJeu()) {
			ImageIcon ii = new ImageIcon(c.getRecto());
			ImageIcon adj = new ImageIcon(ii.getImage().getScaledInstance(988/6, 128, Image.SCALE_SMOOTH));
			JLabel img = new JLabel(adj);
			
			ImageIcon ii1 = new ImageIcon(c.getRecto());
			ImageIcon adj1 = new ImageIcon(ii1.getImage().getScaledInstance(280, 330,Image.SCALE_SMOOTH));
			JLabel img1 = new JLabel(adj1);
			
			img.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					zoom.removeAll();
					zoom.add(img1);
					game.pack();
					game.repaint();
				}
				
			});
			main.add(img);
		}
		
		//Centre - Fusion
		JPanel centre = new JPanel();
		centre.setLayout(new BoxLayout(centre, BoxLayout.Y_AXIS));
		centre.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		centre.add(haut);
		centre.add(jspJeu);
		centre.add(main);
		
		//Fusion totale
		
		sup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Jeu.getInstance().setCritera(0);
				partie.removeAll();
				Jeu.getInstance().sortByCritera();
				for(Carte c : Jeu.getInstance().getLine()) {
					ImageIcon ii = new ImageIcon(c.getVerso());
					ImageIcon adj = new ImageIcon(ii.getImage().getScaledInstance(220, 300, Image.SCALE_SMOOTH));
					JLabel img = new JLabel(adj);
					partie.add(img);
				}
				game.pack();
				game.repaint();
			}
			
		});
		
		pop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Jeu.getInstance().setCritera(1);
				partie.removeAll();
				Jeu.getInstance().sortByCritera();
				for(Carte c : Jeu.getInstance().getLine()) {
					ImageIcon ii = new ImageIcon(c.getVerso());
					ImageIcon adj = new ImageIcon(ii.getImage().getScaledInstance(220, 300, Image.SCALE_SMOOTH));
					JLabel img = new JLabel(adj);
					partie.add(img);
				}
				game.pack();
				game.repaint();
			}
			
		});
		
		pol.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Jeu.getInstance().setCritera(2);
				partie.removeAll();
				Jeu.getInstance().sortByCritera();
				for(Carte c : Jeu.getInstance().getLine()) {
					ImageIcon ii = new ImageIcon(c.getVerso());
					ImageIcon adj = new ImageIcon(ii.getImage().getScaledInstance(220, 300, Image.SCALE_SMOOTH));
					JLabel img = new JLabel(adj);
					partie.add(img);
				}
				game.pack();
				game.repaint();
			}
			
		});
		
		pib.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Jeu.getInstance().setCritera(3);
				partie.removeAll();
				Jeu.getInstance().sortByCritera();
				for(Carte c : Jeu.getInstance().getLine()) {
					ImageIcon ii = new ImageIcon(c.getVerso());
					ImageIcon adj = new ImageIcon(ii.getImage().getScaledInstance(220, 300, Image.SCALE_SMOOTH));
					JLabel img = new JLabel(adj);
					partie.add(img);
				}
				game.pack();
				game.repaint();
			}
			
		});
		
		ok.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				boolean ok = true;
				
				if(jcbMain.getText().isEmpty()) {
					ok = false;
				}
				if(jcbLine.getText().isEmpty()) {
					ok = false;
				}
				
				if(ok) {
					if(!fin) {
						if(Jeu.getInstance().getJoueurs().indexOf(Jeu.getInstance().getTurnPlayer()) == 0) {
							int c = 0;
							for(Joueur j : Jeu.getInstance().getJoueurs()) {
								if(j.getJeu().size() == 0) {
									c++;
								}
							}
							if(c == 1) {
								boolean trouve = false;
								int i = 0;
								while(!trouve) {
									if(Jeu.getInstance().getJoueurs().get(i).getJeu().size() == 0) {
										trouve = true;
									}else {
										Jeu.getInstance().getJoueurs().get(i).setEtat(false);
										i++;
									}
								}
								game.repaint();
								JOptionPane.showMessageDialog( null, Jeu.getInstance().getJoueurs().get(i).getPseudo() + " a gagne ! Bravo !", 
								      "Partie termine", JOptionPane.INFORMATION_MESSAGE);
							}else {
								if(c > 1) {
									for(Joueur j : Jeu.getInstance().getJoueurs()) {
										if(j.getJeu().size() == 0) {
											Jeu.getInstance().distribuerCarte(j);
										}else {
											j.setEtat(false);
										}
									}
									game.repaint();
								}
							}
						}
						
						if(Jeu.getInstance().getTurnPlayer().getEtat()) {
							
							if(Jeu.getInstance().getTurnPlayer() instanceof Bot) {
								try {
									Jeu.getInstance().getTurnPlayer().jouer();
									partie.removeAll();
									for(Carte c : Jeu.getInstance().getLine()) {
										ImageIcon ii = new ImageIcon(c.getVerso());
										ImageIcon adj = new ImageIcon(ii.getImage().getScaledInstance(220, 300, Image.SCALE_SMOOTH));
										JLabel img = new JLabel(adj);
										partie.add(img);
									}
									
									
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else {
								String valInd = (String) jcbMain.getText();
								int indexMain = Integer.parseInt(valInd);
								if(indexMain >= Jeu.getInstance().getTurnPlayer().getJeu().size()) {
									indexMain = Jeu.getInstance().getTurnPlayer().getJeu().size()-1;
								}else {
									if(indexMain < 0) {
										indexMain = 0;
									}
								}
								
								String valLine = (String) jcbLine.getText();
								int indexLine= Integer.parseInt(valLine);
								if(indexLine > Jeu.getInstance().getLine().size()) {
									indexLine = Jeu.getInstance().getLine().size();
								}else {
									if(indexLine < 0) {
										indexLine = 0;
									}
								}							
								
								Carte cM = Jeu.getInstance().getTurnPlayer().getJeu().get(indexMain);
								ArrayList<Carte> copie = new ArrayList<>(Jeu.getInstance().getLine());
								copie.add(indexLine, cM);
								Jeu.getInstance().getLine().add(cM);
								Jeu.getInstance().sortByCritera();
								if(copie.equals(Jeu.getInstance().getLine())) {
									Jeu.getInstance().getTurnPlayer().ajouterPoint();
									partie.removeAll();
									for(Carte c : Jeu.getInstance().getLine()) {
										ImageIcon ii = new ImageIcon(c.getVerso());
										ImageIcon adj = new ImageIcon(ii.getImage().getScaledInstance(220, 300, Image.SCALE_SMOOTH));
										JLabel img = new JLabel(adj);
										partie.add(img);
									}
									Jeu.getInstance().getTurnPlayer().getJeu().remove(indexMain);
								}else {
									Jeu.getInstance().getLine().remove(cM);
									Jeu.getInstance().getTurnPlayer().getJeu().remove(cM);
									Jeu.getInstance().distribuerCarte(Jeu.getInstance().getTurnPlayer());
								}
								
							}
						}
						
						
						Jeu.getInstance().incrementerTurnPlayer();
						jtaS.setText("");
						jtaJ.setText("");
						for(Joueur j : Jeu.getInstance().getJoueurs()) {
							jtaS.setText(jtaS.getText() + j.getPseudo() + " : " + j.getPoints() + "\n");
							jtaJ.setText(jtaJ.getText() + j.getPseudo() + " : " + j.getJeu().size() + " cartes restantes\t");
						}
						main.removeAll();
						for(Carte c : Jeu.getInstance().getTurnPlayer().getJeu()) {
							ImageIcon ii = new ImageIcon(c.getRecto());
							ImageIcon adj = new ImageIcon(ii.getImage().getScaledInstance(988/6, 128, Image.SCALE_SMOOTH));
							JLabel img = new JLabel(adj);
							
							ImageIcon ii1 = new ImageIcon(c.getRecto());
							ImageIcon adj1 = new ImageIcon(ii1.getImage().getScaledInstance(280, 330,Image.SCALE_SMOOTH));
							JLabel img1 = new JLabel(adj1);
							
							img.addMouseListener(new MouseListener() {

								@Override
								public void mouseClicked(MouseEvent e) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void mouseEntered(MouseEvent e) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void mouseExited(MouseEvent e) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void mousePressed(MouseEvent e) {
									// TODO Auto-generated method stub
									
								}

								@Override
								public void mouseReleased(MouseEvent e) {
									zoom.removeAll();
									zoom.add(img1);
									game.pack();
									game.repaint();
								}
								
							});
							main.add(img);
						}
						jta.setText(jta.getText() + "\nAu tour de " + Jeu.getInstance().getTurnPlayer().getPseudo() + " de jouer.");
						if(Jeu.getInstance().getTurnPlayer() instanceof PersonnePhysique) {
							jta.setText(jta.getText() + "\nEntrez la place de la carte de la main (0 pour la premiere) puis" +
									"\nEntrez la place ou inserer votre carte \n"
									+ "0 pour la placer en premiere position.");
						}else {
							jta.setText(jta.getText() + "\nLe joueur est un bot, appuyez sur OK");
						}
						
						game.repaint();
					}
				}
					
			}
				
		});
		
		game.add(gauche, BorderLayout.LINE_START);
		game.add(centre, BorderLayout.CENTER);
		game.add(droite, BorderLayout.LINE_END);
		
		game.setSize(1500, 600);
		game.setResizable(false);
		game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.setVisible(true);
		game.setResizable(false);
		game.setLocationRelativeTo(null);


	}
}
